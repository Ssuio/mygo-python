import json
import re
from datetime import datetime

input_value = {
    'hired': {
        'be': {
            'to': {
                'deserve': 'I'
            }
        }
    }
}

def reverseDict(obj):
  words = []

  while isinstance(obj, dict):
    for k,v in obj.items():
      words.append(k)  
    obj = v

  words.append(obj)

  res = {}
  tmp = res
  l = len(words)
  for i,w in enumerate(reversed(words)):
    if i is l - 2:
      tmp[w] = words[0]
      break
    else:
      tmp[w] = {}
      tmp = tmp[w]   

  return res

def reverseDict2(obj):
  s = json.dumps(obj)
  p = re.compile('\{\"([^"]*)\"')
  words = p.findall(s)
  p2 = re.compile('\"([^"]*)\"}+')
  words.append(p2.findall(s)[0])
  head = ''
  tail = ''
  l = len(words)
  for i,w in enumerate(reversed(words)):
    tail += '}'
    
    if i is l - 2:
      head += '{"' + w + '":"'+ words[0] +'"'
      break
    
    head += '{"' + w + '":'
  return json.loads(head + tail)
  
    
start = datetime.now()
r = reverseDict(input_value)
print('Result 1:', r)
print('Time:', datetime.now() - start)

start = datetime.now()
reverseDict2(input_value)
print('Result 2:', r)
print('Time:', datetime.now() - start)