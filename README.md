# Python3 test
 
# Please write a function to reverse the following nested input value into output value
 
# Input:
input_value = {
  'hired': {
    'be': {
      'to': {
        'deserve': 'I'
      }
    }
  }
}
 
# Output:
output_value = {
  'I': {
    'deserve': {
      'to': {
         'be': 'hired'
      }
    }
  }
}

# Usage

```bash
    python3 main.py
```